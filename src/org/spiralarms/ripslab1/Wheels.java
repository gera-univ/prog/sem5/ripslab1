package org.spiralarms.ripslab1;

/**
 * Represents wheels of a car
 * Wheels have health and wear out each km travelled
 */
public class Wheels {
    private double health;
    private double wear;

    public Wheels(double health, double wear) {
        this.health = health;
        this.wear = wear;
    }

    public double getHealth() {
        return health;
    }

    public double getWear() {
        return wear;
    }

    public void decreaseHealth(double h) {
        health -= h;
    }

    @Override
    public String toString() {
        return "Wheels{" +
                "health=" + health +
                ", wearout=" + wear +
                '}';
    }
}
