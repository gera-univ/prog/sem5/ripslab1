package org.spiralarms.ripslab1;

/**
 * Represents a model (make) of a car
 */
public class CarModel {
    private final String name;
    private final double tankSize;
    private final double fuelConsumtion;

    public CarModel(String name, double tankSize, double fuelConsumtion) {
        this.name = name;
        this.tankSize = tankSize;
        this.fuelConsumtion = fuelConsumtion;
    }

    public double getTankSize() {
        return tankSize;
    }

    public double getFuelConsumtion() {
        return fuelConsumtion;
    }

    @Override
    public String toString() {
        return name;
    }
}
