package org.spiralarms.ripslab1;

import java.util.Scanner;

/**
 * Controls the user interface
 */
public class View {
    Scanner sc;
    Controller c;

    public View() {
        sc = new Scanner(System.in);
        c = new Controller(this);
    }

    public static void main(String[] args) {
        View v = new View();
        v.showActionMenu();
    }

    public void showActionMenu() {
        int choice = -1;
        while (choice != 0) {
            System.out.println(
                    "Choose action\n" +
                            "1) Refuel car\n" +
                            "2) Replace wheels\n" +
                            "3) Perform test drive\n" +
                            "4) Print car information\n" +
                            "0) Quit"
            );
            choice = sc.nextInt();
            switch (choice) {
                case 1 -> c.actionRefuel();
                case 2 -> c.actionReplaceWheels();
                case 3 -> c.actionTestDrive();
                case 4 -> c.actionPrintCarInformation();
            }
        }
    }

    public CarModel askCarModelParameters() {
        System.out.println("Car model ->");
        String name = sc.next();
        System.out.println("Car model tank size ->");
        double tankSize = sc.nextDouble();
        System.out.println("Car model fuel consumption ->");
        double fuelConsumption = sc.nextDouble();
        return new CarModel(name, tankSize, fuelConsumption);
    }

    public Wheels askWheelsParameters() {
        System.out.println("Wheel health ->");
        double health = sc.nextDouble();
        System.out.println("Wheel wear per km ->");
        double wear = sc.nextDouble();
        return new Wheels(health, wear);
    }

    public double askTestDriveDistance() {
        System.out.println("Test drive distance ->");
        return sc.nextDouble();
    }

    public void showWheelWornOutMessage() {
        System.out.println("Wheel is worn out!");
    }

    public void showOutOfFuelMessage() {
        System.out.println("Out of fuel!");
    }

    public void printCarInformation(Car car) {
        System.out.println(car);
    }
}
